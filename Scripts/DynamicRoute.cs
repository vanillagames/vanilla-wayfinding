﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Vanilla.Math;
using Random = UnityEngine.Random;

namespace Vanilla.Wayfinding
{
    public class DynamicRoute : VanillaBehaviour
    {
        public static DynamicRoute i;

        [Header("State")]
        [ReadOnly]
        public bool deployed;

        [Header("Node Settings")]
        public bool doNodeStitching = true;

        public bool deactivatePassedNodes;
        
        [Header("Node Styles")]
        public RouteSegmentStyle colourizationStyle;

        public int staticMaterialIndex;

        public Material[] meshMaterials = new Material[12];
        public Material[] lineMaterials = new Material[12];

        [Header("Route Data")]
        [ReadOnly]
        public List<RouteSegment> segments = new List<RouteSegment>();

        [ReadOnly]
        // This is for amalgamating the NavNodes contributed by other NavNodePools to assemble a given route.
        public List<NavNode> nodes = new List<NavNode>();

        public static UnityEvent onDynamicRouteComplete = new UnityEvent();

        void Awake()
        {
            i = this;
        }

        public void AddToRoute(RouteSegment newSegment)
        {
            if (deployed)
            {
                CleanUp();
            }
            
            segments.Add(newSegment);
        }
        
        public void FormRoute()
        {
            // NavigationRoutes include nodes at their start and destination.
            // In order to not double-up, we want to turn these nodes off if necessary.
            // The conditions for when to do this are kind of specific... see below.
            
            // Node stylization starts here.
            // matID acts as the material index and gets changed if or when we've nominated it to
            // based on the colourizationStyle enum.
            
            // We always start with a random index and then change it as specified during the setup loops below.
            int matID = -1;

            if (colourizationStyle != RouteSegmentStyle.None)
            {
                switch (colourizationStyle)
                {
                    case RouteSegmentStyle.Static:
                        matID = staticMaterialIndex;
                        break;

                    case RouteSegmentStyle.Sequential:
                    case RouteSegmentStyle.Random:
                        matID = GetRandomColourID();
                        break;
                }

                foreach (var s in segments)
                {
                    s.SetLineMaterial(ref lineMaterials[matID]);
                    s.DrawRoute();
                    s.ColourizeNodeMeshes(ref meshMaterials[matID]);

                    if (colourizationStyle != RouteSegmentStyle.Static)
                        matID = GetColourChange(matID);
                }
            }

            if (doNodeStitching)
            {
                // Check through all the segments starting at the first...
                for (int s = 0; s < segments.Count; s++)
                {
                    // If the segment isn't a NavigationRoute, skip it.
                    // We only want to hide superfluous navigation nodes!
                    if (segments[s].type != RouteSegmentType.NavigationRoute) continue;

                    // If this segment isn't the first, and the one behind it isn't a navigation route...
                    // (NavigationRoutes only turn their node off if there's one ahead, not behind!)
                    if (s > 0 && segments[s - 1].type != RouteSegmentType.NavigationRoute)
                    {
                        // ToDo - Here, just before we return it, we should set this nodes respective index in the LineRenderer to be the following node.
                        // ToDo - Otherwise, the LineRenderer still points to this ghost node position.
                        // Set this segments first lineRenderer position to the position of the last node in the previous segment - Untested!
                        segments[s].Line.SetPosition(0,
                            segments[s - 1].nodes[segments[s - 1].nodes.Count - 1].transform.localPosition);

                        // Return the first node
//                        NavNodePool.i.Return(segments[s].nodes[0]);
                        segments[s].nodes[0].Deactivate();
                    }

                    // If this segment isn't the last...
                    if (s < segments.Count - 1)
                    {
                        // ToDo - Here, just before we return it, we should set this nodes respective index in the LineRenderer to be the following node.
                        // ToDo - Otherwise, the LineRenderer still points to this ghost node position.
                        // Set this segments last lineRenderer position to the position of the first node in the following segment - Untested!
                        segments[s].Line.SetPosition(segments[s].Line.RouteRenderer.positionCount - 1,
                            segments[s + 1].nodes[0].transform.localPosition);

                        // Return the last node
//                        NavNodePool.i.Return(segments[s].nodes[segments[s].nodes.Count - 1]);
                        segments[s].nodes[segments[s].nodes.Count - 1].Deactivate();
                    }
                }
            }

            int globalNodeCount = 0;

            // We can safely assume that all remaining nodes that are active should be considered
            // as part of this route, so we add them to our list and 'globally' index them all.
            foreach (var s in segments)
            {
                foreach (var n in s.nodes)
                {
                    if (!n.gameObject.activeSelf) continue;

                    nodes.Add(n);
                    
                    n.globalIndex = globalNodeCount;

                    globalNodeCount++;
                }
            }
            
            // For all except the last, assign each nodes NextNode with the one following it
            // and turn on its 'direction arrow' mesh.
            for (int n = 0; n < globalNodeCount-1; n++)
            {
                nodes[n].SetNextNode(nodes[n+1]);
                
                nodes[n].ChangeToDirection();
            }

            // Routes marked as a loop are treated as normal except if they're the last one.
            // In that case, there's no goal mesh, it just points to the start node.
            if (segments[segments.Count - 1].loop)
            {
                nodes[globalNodeCount-1].SetNextNode(segments[segments.Count-1].nodes[0]);
                
                nodes[globalNodeCount-1].ChangeToDirection();
            }
            else
            {
                nodes[globalNodeCount - 1].ChangeToGoal();
            }

            nodes[0].globalRole = RouteRole.Start;
            
            nodes[globalNodeCount-1].globalRole = RouteRole.End;

            deployed = true;

            nodes[0].BecomeCurrent();

            onDynamicRouteComplete.Invoke();
        }

        public void CleanUp()
        {
            if (!deployed)
            {
                Warning("The DynamicRoute isn't deployed so it can't run CleanUp");
                return;
            }

            foreach (NavNode n in nodes)
            {
                n.DynamicRouteCleanUp();
            }

            foreach (RouteSegment s in segments)
            {
                s.ClearRouteLine();
                
                if (s.type != RouteSegmentType.NavigationRoute) continue;
                
                RouteSegmentPool.i.Return(s);
            }
            
            segments.Clear();
            nodes.Clear();
            
            deployed = false;
        }

//        public void FormLoopedRoute()
//        {
//            
//        }

        // These require DynamicRoute to inherit from Route, which doesn't make too much sense anymore.
//        public void FormLinearRoute(Vector3[] positions)
//        {
//            currentArray = positions;
//
//            BalanceUsed(positions.Length);
//
//            // Index them
//            for (int i = 0; i < used.Count; i++)
//            {
//                used[i].id = i;
//                
//                used[i].transform.position = positions[i];
//            }
//            
//            // Arrange nodes by order
//            for (int i = 0; i < used.Count - 1; i++)
//            {
//                used[i].SetNextNode(used[i + 1]);
//                used[i].ChangeToDirection();
//            }
//
//            // Set the last one as the goal
//            used[used.Count - 1].ChangeToGoal();
//            
//            // Set the first node as 'current'
//            used[0].BecomeCurrent();
//        }
//
//        public void FormLoopedRoute(Vector3[] positions, int loopIndex)
//        {
//            currentArray = positions;
//            
//            BalanceUsed(positions.Length);
//
//            // Index them
//            for (int i = 0; i < used.Count; i++)
//            {
//                used[i].id = i;
//
//                used[i].transform.position = positions[i];
//                
//                used[i].ChangeToDirection();
//            }
//            
//            // Arrange nodes by order
//            for (int i = 0; i < used.Count - 1; i++)
//            {
//                used[i].SetNextNode(used[i + 1]);
//            }
//
//            // Mark the node at the loopIndex as the loopNode
//            used[loopIndex].startOfStaticRoute = true;
//
//            // Tell the last node to direct user to the loop node
//            used[used.Count-1].SetNextNode(used[loopIndex]);
//            
//            // Set the first node as 'current'
//            used[0].BecomeCurrent();
//        }

        // ---------------------------------------------------------------------------------------------------- Helpers

        public int GetRandomColourID()
        {
            return Random.Range(0, meshMaterials.Length - 1);
        }
        
        public int GetColourChange(int matID)
        {
            switch (colourizationStyle)
            {
                case RouteSegmentStyle.Random:
                    return GetRandomColourID();
                        
                case RouteSegmentStyle.Sequential:
                    return matID.GetWrap(matID + 1, meshMaterials.Length - 1);
                
                default:
                    return matID;
            }
        }
    }
}