﻿using System.Collections;
using System.Collections.Generic;
using Mapbox.Unity.Utilities;
using Mapbox.Utils;
using UnityEngine;

[CreateAssetMenu(fileName = "New Mapbox Access Profile", menuName = "+Vanilla/Wayfinding/Mapbox Access Profile")]
public class MapboxAccessProfile : ScriptableObject
{
    public string userName;
    public string dataSetID;
    public string accessToken;

    [SerializeField]
    [Geocode]
    public string editorLatLong;
}