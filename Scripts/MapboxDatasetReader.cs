﻿using System.Collections;

using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;

using SimpleJSON;

namespace Vanilla.Wayfinding
{
    public class MapboxDatasetReader : VanillaBehaviour
    {
        public static MapboxDatasetReader i;

        [Header("Mapbox API Settings")]
        private const string urlSegmentMapboxAPI = "https://api.mapbox.com/";
        private const string urlSegmentDataset = "datasets/v1/";

        private const string urlSegmentAccessTokenParameter = "?access_token=";

        public MapboxAccessProfile currentProfile;
        
        [SerializeField, ReadOnly]
        private string compositeURL;

        public string CompositeURL
        {
            get
            {
                if (!string.IsNullOrEmpty(compositeURL)) return compositeURL;

                compositeURL = $"{urlSegmentMapboxAPI}{urlSegmentDataset}{currentProfile.userName}/{currentProfile.dataSetID}/features{urlSegmentAccessTokenParameter}{currentProfile.accessToken}";

                return compositeURL;
            }
        }

        [Header("Mapbox GeoJSON")]
        public bool refreshing;

        [SerializeField]
        public MapboxJSONCollection jsonCollection;

        [Header("Route Segment Parents")]
        public Transform mapboxPointsParent;
        public Transform mapboxLineRoutesParent;
        public Transform navigationRoutesParent;

        [Header("Events")] 
        public UnityEvent onDataReady = new UnityEvent();
        
        [Header("Debug")]
        public bool mapInitialized;

        void Awake()
        {
            i = this;
        }

        void Start()
        {
            MapSingleton.i.OnInitialized += MapInitialized;
        }

        public void ProfileSelected(MapboxAccessProfile profile)
        {
            currentProfile = profile;
            
            if (mapInitialized) 
                SendMapboxRESTRequest();
        }

        public void MapInitialized()
        {
            mapInitialized = true;

            MapSingleton.i.OnInitialized -= MapInitialized;
            
            if (currentProfile) 
                SendMapboxRESTRequest();
        }

        [ContextMenu("StartProcess")]
        public void SendMapboxRESTRequest()
        {
            if (refreshing) return;
            
            refreshing = true;
                
            StartCoroutine(UpdateFromMapbox());
        }

        private IEnumerator UpdateFromMapbox()
        {
            // ------------------------------------------------------------------------ Check for internet connectivity
            
            WaitForSeconds waitASec = new WaitForSeconds(1.0f);

            while (Application.internetReachability == NetworkReachability.NotReachable)
            {
                yield return waitASec;
            }

            // --------------------------------------------------------------------------------- Contact Mapbox servers

            UnityWebRequest request = UnityWebRequest.Get(CompositeURL);
            
            request.SetRequestHeader("Content-Type", "text/html");

            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Log(($"Request error [{request.responseCode}] - [{request.error}]"));
                yield break;
            }
            
            // ------------------------------------------------------------------------------ Cache GeoJSON information
            
            jsonCollection.points.Clear();
            jsonCollection.lines.Clear();
            jsonCollection.polygons.Clear();

            JSONObject rootJSONObject = (JSONObject)JSONNode.Parse(request.downloadHandler.text);

            foreach (JSONObject feature in rootJSONObject["features"])
            {
                switch (feature["geometry"]["type"].ToString())
                {
                    case "\"Point\"":
                        jsonCollection.points.Add(new MapboxJSONPoint(feature));
                        break;
                    
                    case "\"LineString\"":
                        jsonCollection.lines.Add(new MapboxJSONLine(feature));
                        break;
                    
                    case "\"Polygon\"":
                        Log("I'm a baby program and I don't know how to store or use polygons yet :(");
                        break;
                }
            }
            
            // --------------------------------------------------------------------------- Construct points-of-interest

            foreach (MapboxJSONPoint point in jsonCollection.points)
            {
                if (!point.HasProperty("name"))
                {
                    continue;
                }
                
                RouteSegmentPool.i.Get().InitializeAsMapboxPoint(point);
            }
            
            // --------------------------------------------------------------------------------------- Construct routes

            foreach (MapboxJSONLine line in jsonCollection.lines)
            {
                if (!line.HasProperty("name"))
                {
                    continue;
                }

                RouteSegmentPool.i.Get().InitializeAsMapboxLine(line);
            }
            
            // ------------------------------------------------------------------------------------- Construct polygons

            foreach (MapboxJSONPolygon polygon in jsonCollection.polygons)
            {
                if (!polygon.HasProperty("name"))
                {
                    continue;
                }
            }
            
            // ------------------------------------------------------------------------------------- Invoke ready event
            
            refreshing = false;
            
            onDataReady.Invoke();
        }
    }
}