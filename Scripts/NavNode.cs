﻿using UnityEngine;

namespace Vanilla.Wayfinding
{
    public class NavNode : VanillaBehaviour, IPoolable<NavNode, NavNodePool>
    {
        public static NavNodePool _pool;        
        public NavNodePool pool
        {
            get => _pool;
            set => _pool = value;
        }

        public static NavNode Current;

        public static readonly GenericEvent<NavNode> OnNodeReached = new GenericEvent<NavNode>();
        public static readonly GenericEvent<NavNode> OnNewCurrentNode = new GenericEvent<NavNode>();

        [ReadOnly]
        public int localIndex = -1;

        [ReadOnly]
        public int globalIndex = -1;

        [ReadOnly]
        public RouteSegment segment;

        [ReadOnly]
        public NavNode nextNode;

        public MeshRenderer[] colourMeshes = new MeshRenderer[4];

        [ReadOnly]
        public RouteRole localRole;

        [ReadOnly]
        public RouteRole globalRole;

        [SerializeField, ReadOnly]
        private NavNodeMesh _mesh;

        public NavNodeMesh mesh
        {
            get => _mesh;
            set
            {
                if (_mesh == value) return;

                _mesh = value;

                switch (_mesh)
                {
                    case NavNodeMesh.None:
                        baseMesh.SetActive(false);
                        directionMesh.SetActive(false);
                        goalMesh.SetActive(false);
                        break;

                    case NavNodeMesh.Direction:
                        baseMesh.SetActive(true);
                        directionMesh.SetActive(true);
                        goalMesh.SetActive(false);
                        break;

                    case NavNodeMesh.Goal:
                        baseMesh.SetActive(true);
                        directionMesh.SetActive(false);
                        goalMesh.SetActive(true);
                        break;
                }

//                #if UNITY_EDITOR || DEVELOPMENT_BUILD
//                    gameObject.name = $"NavNode [{(currentFeature ? currentFeature.name : "-")}] [{localIndex}] [{_mesh}]";
//                #endif
            }
        }

        public GameObject baseMesh;
        public GameObject directionMesh;
        public GameObject goalMesh;
        
        // ----------------------------------------------------------------------------------------------- Initializers

        [ContextMenu("Deactivate [ NavNode ]")]
        public override void Deactivate()
        {
            Log("Deactivated");
            
            base.Deactivate();

            if (segment)
            {
                segment.DrawRoute();
            }
        }

        // ----------------------------------------------------------------------------------------------- Initializers

        public void InitializeSegment(int LocalIndex, RouteSegment Segment, Vector3 position, RouteRole LocalRole)
        {
            segment = Segment;

            localRole = LocalRole;

            segment.nodes.Add(this);

            localIndex = LocalIndex;

            gameObject.name = $"NavNode [{Segment.type.ToString()}] [{LocalIndex}] [{Segment.name}]";

            transform.SetParent(segment.transform);

            transform.position = position;
        }

        public void BecomeCurrent()
        {
            if (Current == this) return;

            if (DynamicRoute.i.deactivatePassedNodes)
            {
                if (globalIndex > 1)
                {
                    DynamicRoute.i.nodes[globalIndex-2].Deactivate();
                }
            }

            Current = this;
            
            OnNewCurrentNode.Invoke(this);
        }

        public virtual void NodeReached()
        {
            Log(
                $"Node reached! [{segment.name}]/n" +
                $"Local [{localRole}] [{localIndex}]/n" +
                $"Global [{globalRole}] [{globalIndex}]");
            
            OnNodeReached.Invoke(this);

            if (nextNode)
            {
                nextNode.BecomeCurrent();
            }
        }

        public void SetNextNode(NavNode node)
        {
            nextNode = node;
            
            transform.LookAt(nextNode.transform);
        }
        
        [ContextMenu("Change to Unused")]
        public void ChangeToUnused()
        {
            mesh = NavNodeMesh.None;
            
            localIndex = -1;
        }        

        [ContextMenu("Change to Active Direction")]
        public void ChangeToDirection()
        {
            mesh = NavNodeMesh.Direction;
        }

        [ContextMenu("Change to Active Goal")]
        public void ChangeToGoal()
        {
            mesh = NavNodeMesh.Goal;
        }

        [ContextMenu("Test Colourize")]
        public void TestColourize()
        {
            ColourizeMeshes(ref DynamicRoute.i.meshMaterials[UnityEngine.Random.Range(0,11)]);
        }

        public void ColourizeMeshes(ref Material newMaterial)
        {
            foreach (MeshRenderer m in colourMeshes)
            {
                m.sharedMaterial = newMaterial;
            }
        }
        
        // --------------------------------------------------------------------------------------------------- Clean Up

        public void DynamicRouteCleanUp()
        {
            globalIndex = -1;

            globalRole = RouteRole.None;
            
            ChangeToUnused();
        }

//        public void OnGet<T>(VanillaPool<T> pool) where T : VanillaBehaviour, IPoolable
//        {
//            
//        }
//
//        public void OnReturn<T>(VanillaPool<T> pool) where T : VanillaBehaviour, IPoolable
//        {
//            Reset();
//        }


        public void OnGet()
        {
            
        }

        public void OnReturn()
        {
            Reset();
        }

        public override void Reset()
        {
            localIndex = -1;
            globalIndex = -1;

            if (segment)
            {
                segment.nodes.Remove(this);
            }
            
            segment = null;
            nextNode = null;

            localRole = RouteRole.None;
            globalRole = RouteRole.None;
            
            ChangeToUnused();
        }
    }
}