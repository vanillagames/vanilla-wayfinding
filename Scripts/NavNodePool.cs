﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

namespace Vanilla.Wayfinding
{
    public class NavNodePool : VanillaPool<NavNode, NavNodePool>
    {
        public static NavNodePool i;

        void Awake()
        {
            i = this;
        }
        
        [ContextMenu("Fill")]
        public override void Fill()
        {
            base.Fill();
        }
        
        [ContextMenu("Empty")]
        public override void Empty()
        {
            base.Empty();
        }
        
        [ContextMenu("Trim")]
        public override void Trim()
        {
            base.Trim();
        }

        public override void Reset()
        {
            
        }
    }
}