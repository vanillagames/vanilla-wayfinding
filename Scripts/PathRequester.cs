﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

using Mapbox.Directions;
using Mapbox.Unity.Map;
using Mapbox.Unity.Utilities;
using Mapbox.Utils;

using Mapbox.Unity;

namespace Vanilla.Wayfinding
{
    public class PathRequester : VanillaBehaviour
    {
        
        public static PathRequester i;

        // ------------------------------------------------------------------------------------------------ AbstractMap

        [SerializeField, ReadOnly] private AbstractMap map;
        private AbstractMap Map => map ? map : map = MapSingleton.i;

        // --------------------------------------------------------------------------------------------- User Transform

        [SerializeField, ReadOnly] private Transform user;
        public Transform User => user ? user : (user = Player.t);

        // ------------------------------------------------------------------------------------------------- Directions

        private Directions _mapBoxDirections;

        private Directions MapBoxDirections =>
            _mapBoxDirections ?? (_mapBoxDirections = MapboxAccess.Instance.Directions);

        // ---------------------------------------------------------------------------------------- Directions Resource

        private DirectionResource _directionResource;

        private DirectionResource DirectionsResource =>
            _directionResource ?? (_directionResource = new DirectionResource(
                coordinates: new Vector2d[2],
                profile: RoutingProfile.Walking)
            );

        // ---------------------------------------------------------------------------------------- Directions Response

        [SerializeField]
        public DirectionsResponse navigationResponse;
        
        // ---------------------------------------------------------------------------------------- Navigation Handling

        public List<Transform> navigationTargets = new List<Transform>();


        public List<Vector3> navigationResults = new List<Vector3>();


        public IEnumerator compoundNavigationProcess;

        [ReadOnly]
        public bool compoundNavigationInProgress;

        [ReadOnly]
        public bool waitingForResponse;

        public float defaultResponseTimeout = 10.0f;

        // ----------------------------------------------------------------------------------------------------- Events

        public GenericEvent<List<Vector3>> OnNavigationReady = new GenericEvent<List<Vector3>>();

        // --------------------------------------------------------------------------------------------- Initialization

        private void Awake()
        {
            i = this;

            // This was set to true initially, it's only off for testing

            // directionsResource.Steps = true;
            DirectionsResource.Steps = false;
        }

        // ----------------------------------------------------------------------------------------- Navigation Request

        /// <summary>
        /// Adds a callback to the onNavigationReady event.
        /// </summary>
        /// <param name="callback"></param>
        public void AddCallback(UnityAction<List<Vector3>> callback)
        {
            if (callback != null)
                OnNavigationReady.AddListener(callback);
        }

        // ------------------------------------------------------------------------------------------ Direct Navigation

        /// <summary>
        /// Directly returns 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="callback"></param>
        public void RequestDirectNavigation(Transform from, Transform to, UnityAction<List<Vector3>> callback)
        {
            if (waitingForResponse)
            {
                Warning("Navigation is already taking place, please wait...");
                return;
            }

            if (SilentNullCheck(from)) return;
            if (SilentNullCheck(to)) return;

            AddCallback(callback);
            
            navigationResults.Clear();

            PrepareNavigation(from, to);

            MapBoxDirections.Query(DirectionsResource, HandleDirectNavigationResponse);

            waitingForResponse = true;
        }

        /// <summary>
        /// This handler will clear the navigationResults list first, then add the received positions,
        /// then invoke the onNavigationReady event and finally remove all listeners.
        /// </summary>
        private void HandleDirectNavigationResponse(DirectionsResponse response)
        {
            waitingForResponse = false;

            ProcessResponse(response);

            FinishNavigation();
        }

        // ---------------------------------------------------------------------------------------- Compound Navigation

        /// <summary>
        /// Adds a transform to the list of targets to be navigated between during RunCompoundNavigation();
        /// </summary>
        /// <param name="t"></param>
        public void AddNavigationTarget(Transform t)
        {
            if (SilentNullCheck(t)) return;

            navigationTargets.Add(t);
        }

        /// <summary>
        /// Removes a specific transform from the list of navigationTargets.
        /// </summary>
        /// <param name="t"></param>
        public void RemoveNavigationTarget(Transform t)
        {
            if (SilentNullCheck(t)) return;

            navigationTargets.Remove(t);
        }

        public void RemoveAllNavigationTargets()
        {
            navigationTargets.Clear();
        }

        /// <summary>
        /// Begins a coroutine which will automatically cycle through all the transforms in the navigationTargets list,
        /// querying the MapBox Directions API and adding the positions received therein when they come through.
        /// At least 2 transforms must be added before beginning.
        /// </summary>
        public void RequestCompoundNavigation(UnityAction<List<Vector3>> callback)
        {
            if (compoundNavigationInProgress)
            {
                Warning("Navigation is already taking place, please wait...");
                return;
            }

            if (navigationTargets.Count < 2)
            {
                Warning("Two or more navigation target transforms are required before navigation can be calculated.");
                return;
            }

            if (compoundNavigationProcess != null)
            {
                StopCoroutine(compoundNavigationProcess);
            }

            compoundNavigationProcess = CompoundNavigationProcess();

            AddCallback(callback);

            navigationResults.Clear();

            compoundNavigationInProgress = true;

            StartCoroutine(compoundNavigationProcess);
        }

        /// <summary>
        /// Iterates through all the transforms in navigationTargets and adds all the positions from each response,
        /// yielding between operations.
        /// We include a timeout tickdown in the event of no response.
        /// Only one compound navigation can be run at a time; other requests will be knocked back.
        /// </summary>
        /// <returns></returns>
        private IEnumerator CompoundNavigationProcess()
        {
            int i = 0;

            while (i < navigationTargets.Count - 1)
            {
                PrepareNavigation(navigationTargets[i], navigationTargets[i + 1]);

                MapBoxDirections.Query(DirectionsResource, HandleCompoundNavigationResponse);

                waitingForResponse = true;

                var timeout = defaultResponseTimeout;

                while (waitingForResponse)
                {
                    timeout -= Time.deltaTime;

                    if (timeout < 0)
                    {
                        Error("Time-out while waiting for a navigation response from MapBox.");

                        waitingForResponse = false;

                        compoundNavigationInProgress = false;

                        OnNavigationReady.RemoveAllListeners();

                        yield break;
                    }

                    yield return null;
                }

                i++;
            }

            compoundNavigationInProgress = false;

            FinishNavigation();
        }

        /// <summary>
        /// This handler will simply add on the received positions and turn off waitingForResponse.
        /// </summary>
        private void HandleCompoundNavigationResponse(DirectionsResponse response)
        {
            waitingForResponse = false;

            ProcessResponse(response);
        }

        // ---------------------------------------------------------------------------------------------------- Helpers

        /// <summary>
        /// Invokes the onNavigationReady event and resets required values.
        /// </summary>
        private void FinishNavigation()
        {
            OnNavigationReady.Invoke(navigationResults);

            OnNavigationReady.RemoveAllListeners();

            navigationTargets.Clear();
        }

        /// <summary>
        /// Checks that the response isn't improperly formed, and then adds the positions within
        /// to the navigationResults list.
        /// </summary>
        private void ProcessResponse(DirectionsResponse response)
        {
            navigationResponse = response;
            
            if (NavigationResponseIsInvalid(ref response)) return;

            foreach (var v2 in response.Routes[0].Geometry)
            {
                navigationResults.Add(LatLongToVector3(v2));
            }
        }

        /// <summary>
        /// Returns true if the received response is unusable.
        /// </summary>
        private bool NavigationResponseIsInvalid(ref DirectionsResponse response)
        {
            if (response?.Routes == null || response.Routes.Count < 1)
            {
                Error("The response received from Mapbox was improperly formed or contained no usable data.");
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Converts a LatLong to a Vector3 relative to the abstract maps transform in world space.
        /// </summary>
        private Vector3 LatLongToVector3(Vector2d vector2D)
        {
            return Map.transform.TransformPoint(
                Conversions.GeoToWorldPosition(
                    lat: vector2D.x,
                    lon: vector2D.y,
                    refPoint: Map.CenterMercator,
                    scale: Map.WorldRelativeScale
                ).ToVector3xz());
        }

        /// <summary>
        /// Converts the positions of both transforms to latitude/longitudes for use with a MapBox Directions query.
        /// </summary>
        private void PrepareNavigation(Transform from, Transform to)
        {
            DirectionsResource.Coordinates[0] = from.GetGeoPositionOrigin(Map.CenterMercator, Map.WorldRelativeScale);
            DirectionsResource.Coordinates[1] = to.GetGeoPositionOrigin(Map.CenterMercator, Map.WorldRelativeScale);
        }
    }
}