﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Vanilla.Wayfinding;

public class ProfileSelector : MonoBehaviour
{
    public MapboxAccessProfile kunshanProfile;
    public MapboxAccessProfile melbourneProfile;

    public void UseKunshanProfile()
    {
        MapboxDatasetReader.i.ProfileSelected(kunshanProfile);
    }

    public void UseMelbourneProfile()
    {
        MapboxDatasetReader.i.ProfileSelected(melbourneProfile);
    }
}