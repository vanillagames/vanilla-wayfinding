﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla.Wayfinding;

[RequireComponent(typeof(LineRenderer))]
public class RouteDrawer : MonoBehaviour
{
    private LineRenderer routeRenderer;
    public LineRenderer RouteRenderer => routeRenderer ? routeRenderer : routeRenderer = GetComponent<LineRenderer>();

    public void DrawLinearLine(ref List<NavNode> route)
    {
        RouteRenderer.positionCount = route.Count;

        //draw the route before the last one
        for (int i = 0; i < route.Count; i++) {
            if (!route[i].gameObject.activeSelf) continue;

            SetPosition(i, route[i].transform.localPosition);
        }
    }

    // should listen to "route" trigger event? Dont need the bool input eventually 
    public void DrawLoopedLine(ref List<NavNode> route)
    {
        RouteRenderer.positionCount = route.Count + 1;

        Vector3 p = Vector3.zero;
            
        //draw the route before the last one
        for (int i = 0; i < route.Count; i++)
        {
            if (!route[i].gameObject.activeSelf) continue;
            
            SetPosition(i, route[i].transform.localPosition);
        }
        
        SetPosition(route.Count, route[0].transform.localPosition);
    }

    public void SetPosition(int index, Vector3 targetLocalPosition)
    {
        RouteRenderer.SetPosition(index, Swizzle(targetLocalPosition));
    }

    // Moves z component => y component and adds a small amount of height purely for a local LineRenderer
    // that has been rotated by 90 degrees.
    private Vector3 Swizzle(Vector3 p)
    {
        return new Vector3(p.x, p.z, -0.01f);
    }

//    public void AddLoopPoint(int index)
//    {
//        RouteRenderer.positionCount++;
//
//        RouteRenderer.SetPosition(RouteRenderer.positionCount - 1, RouteRenderer.GetPosition(index));
//    }
}
