﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Vanilla.Math;

namespace Vanilla.Wayfinding
{
//    [RequireComponent(typeof(RouteDrawer))]
    public class RouteSegment : VanillaBehaviour, ISelectable, IPoolable<RouteSegment, RouteSegmentPool>
    {
        // ------------------------------------------------------------------------------------------------------- Data

        public static RouteSegmentPool _pool;

        public RouteSegmentPool pool
        {
            get => _pool;
            set => _pool = value;
        }

        public new string name;

        public string description;

        public string featureID;

        public bool loop;

        public bool startAnywhere;

        public RouteSegmentType type;

        [SerializeField]
        private RouteDrawer line;
        public RouteDrawer Line => line ? line : (line = GetComponentInChildren<RouteDrawer>());

        public List<NavNode> nodes = new List<NavNode>();

        public static readonly GenericEvent<RouteSegment> OnRouteSegmentComplete = new GenericEvent<RouteSegment>();
        
        // ----------------------------------------------------------------------------------------------- Initializers
        
        public void InitializeAsMapboxPoint(MapboxJSONPoint jsonObject)
        {
            transform.SetParent(MapboxDatasetReader.i.mapboxPointsParent);

            jsonObject.TryGetString("name", ref name);

            jsonObject.TryGetString("description", ref description);
            
            featureID = jsonObject.featureID;

            gameObject.name = $"Mapbox Point [{name}]";
            
            type = RouteSegmentType.MapboxPoint;
            
            NavNodePool.i.Get().InitializeSegment(0, this, jsonObject.position, RouteRole.End);
        }
        
        public void InitializeAsMapboxLine(MapboxJSONLine jsonObject)
        {
            transform.SetParent(MapboxDatasetReader.i.mapboxLineRoutesParent);
            
            jsonObject.TryGetString("name", ref name);

            jsonObject.TryGetString("description", ref description);
            
            featureID = jsonObject.featureID;

            loop = jsonObject.BoolPropertyIsPresentAndTrue("loop");
            
            startAnywhere = jsonObject.BoolPropertyIsPresentAndTrue("startAnywhere");
            
            gameObject.name = $"Mapbox Line [{name}]";
            
            type = RouteSegmentType.MapboxLine;

            InitializeNodes(ref jsonObject.positions);
        }

        public void InitializeAsNavigationRoute(List<Vector3> positions)
        {
            transform.SetParent(MapboxDatasetReader.i.navigationRoutesParent);
            
            name = "Navigation Route Segment";

            description = "Gets you from A to B";

            featureID = "-";
            
            gameObject.name = "Navigation Route Segment";
            
            type = RouteSegmentType.NavigationRoute;
            
            InitializeNodes(ref positions);
        }
        

        // ---------------------------------------------------------------------------------------------- Node Controls

        public void InitializeNodes(ref List<Vector3> positions)
        {
            for (int i = 0; i < positions.Count; i++)
            {
                NavNodePool.i.Get().InitializeSegment(
                    LocalIndex: i, 
                    Segment: this, 
                    position: positions[i], 
                    LocalRole: GetRouteRole(i, positions.Count - 1)
                );
            }
        }

        public void InitializeNodes(ref Vector3[] positions)
        {
            for (int i = 0; i < positions.Length; i++)
            {
                NavNodePool.i.Get().InitializeSegment(
                    LocalIndex: i,
                    Segment: this,
                    position: positions[i],
                    LocalRole: GetRouteRole(i, positions.Length - 1)
                );
            }
        }
        
        public void ReturnCurrentNodes()
        {
            foreach (var n in nodes)
            {
                NavNodePool.i.Return(n);
            }
        }
        
        // -------------------------------------------------------------------------------------------------- Selection

        public void Select()
        {
            RequestNavigation();
        }

        public void Deselect()
        {

        }
        
        // ------------------------------------------------------------------------------------------------- Navigation

        public void RequestNavigation()
        {
            switch (type)
            {
                case RouteSegmentType.MapboxPoint:
                    PathRequester.i.RequestDirectNavigation(Player.t, nodes[0].transform, HandleDemonstrationNavigationResponse);
                    break;
                
                case RouteSegmentType.MapboxLine:
                    var t = startAnywhere ?
                        VanillaMath3D.GetNearestFromList(nodes, Player.t.position).transform :
                        VanillaMath3D.GetNearestOfTwo(nodes[0].transform, nodes[nodes.Count-1].transform, Player.t.position);
                    
                    PathRequester.i.RequestDirectNavigation(Player.t, t, HandleDemonstrationNavigationResponse);
                    break;                
                
                case RouteSegmentType.NavigationRoute:
                    break;
                
                default:
                    return;
            }

            //        // Once the PathRequester has gotten back to us and the NavNodes have been positioned,
//        // let this function know that we're ready to go.
//        customRouteReceiver = CustomRouteReceiver;
//        
//        PathRequester.i.NavigateFromTo(a, b, HandleNavigationResponse);
            
//            RouteSegmentPool.i.Get().InitializeAsNavigationRoute();
//            CustomRoutePool.i.Get().GetNavigation(Player.t, t, HandleCustomRouteResponse);
        }
        // ----------------------------------------------------------------------------------------------- Nav Handlers

        public void HandleDemonstrationNavigationResponse(List<Vector3> navigationPositions)
        {
            RouteSegment r = RouteSegmentPool.i.Get();
            
            r.InitializeAsNavigationRoute(navigationPositions);
            
            DynamicRoute.i.AddToRoute(r);
            
            DynamicRoute.i.AddToRoute(this);
            
            DynamicRoute.i.FormRoute();
        }
        
        // -------------------------------------------------------------------------------------------------- Colourize

//        public void SetLinePositions(ref Material lineMaterial)
//        {
//            if (type != RouteSegmentType.MapboxPoint)
//            {
//                // ToDo Change this back to lineMaterial, I'm just testing - Lucas
//                
//                // ToDo - Can't we just use the LineRenderers tint colour for this instead of entire materials?
//                DrawRoute(ref lineMaterial);
//            }
//        }
        
        public void ColourizeNodeMeshes(ref Material meshMaterial)
        {
            foreach (NavNode n in nodes)
            {
                n.ColourizeMeshes(ref meshMaterial);
            }
        }

        public void ReturnInactiveNodes()
        {
            foreach (NavNode n in nodes)
            {
                if (!n.gameObject.activeSelf) NavNodePool.i.Return(n);
            }
        }

        public void SetLineMaterial(ref Material lineMaterial)
        {
            Line.RouteRenderer.sharedMaterial = lineMaterial;
        }

        public void DrawRoute()
        {
            Log("I'm drawin!");
            
//            ReturnInactiveNodes();

            if (nodes.Count < 2)
            {
                line.enabled = false;
                return;
            }

            line.enabled = true;

            // If we have any inactive nodes at this point, return them.
            // Otherwise, our line will be directing them to a node that isn't visible.

            // ToDo - Can't we just use the LineRenderers tint colour for this instead of entire materials?
//            Line.RouteRenderer.sharedMaterial = c;
//            Line.RouteRenderer.color            

            if (loop)
            {
                Line.DrawLoopedLine(ref nodes);
            }
            else
            {
                Line.DrawLinearLine(ref nodes);
            }
        }

        public void ClearRouteLine()
        {
            Line.RouteRenderer.positionCount = 0;
        }
        
        
        // --------------------------------------------------------------------------------------------------- Clean Up

//        public void OnGet<T>(VanillaPool<T> pool) where T : VanillaBehaviour, IPoolable
//        {
//
//        }
//
//        public void OnReturn<T>(VanillaPool<T> pool) where T : VanillaBehaviour, IPoolable
//        {
//            Reset();
//        }


        public void OnGet()
        {
            
        }

        public void OnReturn()
        {
            Reset();
        }

        public override void Reset()
        {
            name = description = featureID = string.Empty;

            loop = startAnywhere = false;
            
            type = RouteSegmentType.None;
            
            ClearRouteLine();
            
            OnRouteSegmentComplete.RemoveAllListeners();
            
            while (nodes.Count > 0)
            {
                NavNodePool.i.Return(nodes[0]);
            }
            
            nodes.Clear();
        }
        
        // ---------------------------------------------------------------------------------------------------- Helpers

        public RouteRole GetRouteRole(int index, int maximum)
        {
            return index == 0 ? RouteRole.Start : index == maximum ? RouteRole.End : RouteRole.Middle;
        }
    }
}