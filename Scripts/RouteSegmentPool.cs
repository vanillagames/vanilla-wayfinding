﻿using System.Collections;
using System.Collections.Generic;
using Mapbox.Directions;
using UnityEngine;
using Vanilla;
using Vanilla.Wayfinding;

public class RouteSegmentPool : VanillaPool<RouteSegment, RouteSegmentPool>
{
    public static RouteSegmentPool i;

    private void Awake()
    {
        i = this;
    }
        
    [ContextMenu("Fill")]
    public override void Fill()
    {
        base.Fill();
    }

    [ContextMenu("Empty")]
    public override void Empty()
    {
        base.Empty();
    }

    [ContextMenu("Trim")]
    public override void Trim()
    {
        base.Trim();
    }

    public override void Reset()
    {
        
    }
}
