﻿using System.Collections;
using System.Collections.Generic;
using Mapbox.Unity.Map;
using UnityEngine;

namespace Vanilla.Wayfinding
{
    public class MapSingleton : VanillaBehaviour
    {
        public static AbstractMap i;

        void Awake()
        {
            i = GetComponent<AbstractMap>();
        }
    }
}