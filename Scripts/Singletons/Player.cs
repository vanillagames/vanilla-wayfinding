﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vanilla.Wayfinding
{
    public class Player : VanillaBehaviour
    {
        public static Player i;
        public static Transform t;

        private void Awake()
        {
            i = this;
            t = transform;
        }
    }
}