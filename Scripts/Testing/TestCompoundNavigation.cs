﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla.Wayfinding;
   
public class TestCompoundNavigation : MonoBehaviour
{
    public float size = 1.0f;

    public Transform directFrom;
    public Transform directTo;
    
    public Transform[] targets;

    public List<Vector3> results = new List<Vector3>();
    
    [ContextMenu("Test Compound Navigation")]
    public void TestCompoundNav()
    {
        // This works!
        foreach (var t in targets)
        {
            PathRequester.i.AddNavigationTarget(t);
        }
        
        PathRequester.i.RequestCompoundNavigation(ReceiveResults);
    }
        
    [ContextMenu("Test Direct Navigation")]
    public void TestDirectNav()
    {
        PathRequester.i.RequestDirectNavigation(directFrom, directTo, ReceiveResults);
    }

    [ContextMenu("Test Remove Nav Targets")]
    public void TestRemoveTargets()
    {
        PathRequester.i.RemoveAllNavigationTargets();
    }

    public void ReceiveResults(List<Vector3> Results)
    {
        results = Results;
    }

    public void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            Gizmos.color = Color.cyan;

            foreach (var v in results)
            {
                Gizmos.DrawSphere(v, size);
            }
        }
    }
}