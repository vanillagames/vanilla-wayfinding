﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

using Mapbox.Unity.Utilities;
using Mapbox.Utils;

using SimpleJSON;

namespace Vanilla.Wayfinding
{
    public static class MapboxHelpers
    {
        public static Vector3 LatLongToPosition(double latitude, double longitude)
        {
            return MapSingleton.i.transform.TransformPoint(
                Conversions.GeoToWorldPosition(
                        latitude, 
                        longitude,
                        MapSingleton.i.CenterMercator,
                        MapSingleton.i.WorldRelativeScale
                    ).ToVector3xz()
                );
        }

        public static Vector2d PositionToLatLong(this Transform target)
        {
            return target.GetGeoPositionOrigin(MapSingleton.i.CenterMercator, MapSingleton.i.WorldRelativeScale);
        }
    }

    [Serializable]
    public class MapboxJSONObject
    {
        [SerializeField] public string name;

        [SerializeField] public string description;
        
        [SerializeField] public string featureID;

        [SerializeField] public Dictionary<string, string> properties;

        public void Init(JSONObject o)
        {
            featureID = o["id"];

            properties = new Dictionary<string, string>();

            foreach (string k in o["properties"].Keys)
            {
                properties.Add(k, o["properties"][k]);
            }

            TryGetString("name", ref name);
            TryGetString("description", ref description);
        }

        public bool TryGetString(string key, ref string value)
        {
            bool contains = HasProperty(key);

            value = contains ? properties[key] : "-";
            
            return contains;
        }
        
        public bool TryGetInt(string key, ref int value)
        {
            bool contains = HasProperty(key);

            value = contains ? int.Parse(properties[key]) : -1 ;

            return contains;
        }
        
        /// <summary>
        /// Returns true if a property by the name 'key' is present in Mapbox Studio and its value is simply '1';
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool BoolPropertyIsPresentAndTrue(string key)
        {
            if (!HasProperty(key)) return false;
            
            bool parsed = int.TryParse(properties[key], out int value);
            
            return parsed && value == 1;
        }

        public bool HasProperty(string key)
        {
            return properties.ContainsKey(key);
        }
    }
    
    [Serializable]
    public class MapboxJSONPoint : MapboxJSONObject
    {
        [SerializeField] public Vector3 position;

        public MapboxJSONPoint(JSONObject o)
        {
            Init(o);
            
            // These needed flipping!
            // The first entry is actually longitude and the second is latitude
            position = MapboxHelpers.LatLongToPosition(
                o["geometry"]["coordinates"][1],
                o["geometry"]["coordinates"][0]    
            );
        }
    }

    [Serializable]
    public class MapboxJSONLine : MapboxJSONObject
    {
        [SerializeField] public List<Vector3> positions;

        [Tooltip("If true, the route is treated as a closed loop.")]
        [SerializeField] public bool loop;

        [Tooltip("If true, custom navigation to this route will .")]
        [SerializeField] public bool startAnywhere;

        public MapboxJSONLine(JSONObject o)
        {
            Init(o);

            loop = BoolPropertyIsPresentAndTrue("loop");
            
            startAnywhere = BoolPropertyIsPresentAndTrue("startAnywhere");
            
            positions = new List<Vector3>();

            for (int i = 0; i < o["geometry"]["coordinates"].Count; i++)
            {
                positions.Add(
                    MapboxHelpers.LatLongToPosition(
                        o["geometry"]["coordinates"][i][1],
                        o["geometry"]["coordinates"][i][0]
                    )
                );
            }
        }
    }

    [Serializable]
    public class MapboxJSONPolygon : MapboxJSONObject
    {
        
    }
    
    [Serializable]
    public struct MapboxJSONCollection
    {
        [SerializeField]
        public List<MapboxJSONPoint> points;

        [SerializeField]
        public List<MapboxJSONLine> lines;

        [SerializeField]
        public List<MapboxJSONPolygon> polygons;
    }
    
//    public class NavNodeEvent : UnityEvent<NavNode> {}
    
//    public class RouteSegmentEvent : UnityEvent<RouteSegment> {}
//    
//    public class Vector3ListEvent : UnityEvent<List<Vector3>> {}

    public enum NavNodeMesh
    {
        None,
        Direction,
        Goal
    }
    
    public enum RouteRole
    {
        None,
        Start,
        Middle,
        End
    }
    
    public enum RouteSegmentType
    {
        None,
        MapboxPoint,
        MapboxLine,
        NavigationRoute
    }

    public enum RouteSegmentStyle
    {
        None,           // Never alters the nodes materials
        Static,         // Only uses the material at index staticMaterialIndex
        Random,         // Makes each route segment randomly pick from the material list
        Sequential      // Makes each route segment iterate through the material list
    }

    public interface INavigable
    {
        void ConstructRoute(Vector3[] array);
    }
}